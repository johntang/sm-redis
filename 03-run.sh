#docker rm $(docker ps -qa)
docker rm -f soccermom-redis || true

dataDir=`pwd`
dataDir=${dataDir}/data

mkdir -p ${dataDir} || true

docker run -d --name soccermom-redis \
-p 6379:6379 \
-v ${dataDir}:/data \
soccermom/sm-redis redis-server --appendonly yes

echo install redis cli    :  sudo apt-get install redis-tools
echo get the IP address of redis: docker inspect soccermom-redis
echo test redis connection:  redis-cli
echo test redis connection:  redis-cli -h 172.17.0.2

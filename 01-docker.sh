#!/bin/bash
rm -rf build || true
mkdir build
cp Dockerfile build
cp redis.conf build
cd build
docker build -t soccermom/sm-redis .
